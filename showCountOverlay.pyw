# Autoquicker
# Copyright © 2022 eyekay.

import tkinter as tk

top = tk.Tk()

def main():
	top.configure(bg='black')
	top.wm_overrideredirect(True)
	top.wait_visibility(top)
	top.geometry("{0}x{1}+0+0".format(top.winfo_screenwidth(), top.winfo_screenheight()))
	top.attributes('-alpha',0.4)
	l = tk.Label(text=10, font=("Sans", 60))
	l.pack(expand=True)
	l.after(1000, lambda: l.configure(text=9))
	l.after(2000, lambda: l.configure(text=8))
	l.after(3000, lambda: l.configure(text=7))
	l.after(4000, lambda: l.configure(text=6))
	l.after(5000, lambda: l.configure(text=5))
	l.after(6000, lambda: l.configure(text=4))
	l.after(7000, lambda: l.configure(text=3))
	l.after(8000, lambda: l.configure(text=2))
	l.after(9000, lambda: l.configure(text=1))
	l.after(10000, lambda: top.destroy())
	top.mainloop()

if __name__ == "__main__":
    main()
