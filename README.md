This project came about because my development environment was so messed up that I didn't realise that despite the system settings app somehow saying I was on Wayland, I was actually on X11.

This project was dead from the get go, as **it is impossible to create an autoclicker for Wayland because Wayland does not let third party applications simulate input for 'security' reasons.**

Some desktop environments like KDE give an option to override this in which case you can simply use an X11-based autocliker.
