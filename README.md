# ![Autoquicker logo](ico.png) Autoquicker

Autoquicker is an auto clicker. It can repeatedly simulate mouse clicks and key presses on Linux (X11/Wayland) and Windows.

![Screen Shot of Autoquicker's main window](Autoquicker_Screenshot.png)

## Installation

I am working on Windows binaries and AppImages.

## Usage

There are 2 modes: Simple and Pattern. Work on pattern mode has not yet started.

### Simple Mode

Simple mode is fairly straightforward. You can choose whether to automate the mouse or the keyboard. For keyboard, you can choose either which key to press, or what text to type. For mouse automation, you can choose which key to press, how many clicks to execute and the clicking position (you can manually enter the coordinates or interactively choose a position.) For both modes, you can select the time interval between repeats in precision uptop milliseconds. The same goes for scheduling how long the actions should be repeated. You can stop execution anytime by pressing shift+F8.

### Pattern mode

Pattern mode will be used to make and execute 'patterns': pre-determined sequences of actions in the form of a list which can be copied and shared. Documentation will be done after I write it in the first place ;)

## Building

Dependencies:
- python3
- PyQt5
- keyboard module
- PyAutoGui
- Tkinter

Once these are installed, run Autoquicker.py (as root on Linux).

On Windows, you can use PyInstaller to build Autoquicker using the spec file.

## TODO

- [ ] Typing speed (easy)
- [ ] Settings page (hard)
- [ ] Rewrite overlays in Qt to remove Tkinter dependency (very hard)
- [ ] Make it possible to use the desktop in the background while overlay is running (hard) **or** add ability to add delay to automation (easy)
- [ ] _Pattern mode_ (hard overall)
  -   [ ] Show all saved patterns (easy)
  -   [ ] GUI for making patterns (easy)
  -   [ ] GUI for adding actions to patterns (hard)
  -   [ ] Read patterns and write a pattern interpreter (hard)
  -   [ ] Handle editing patterns (easy)
- [ ] Set countdown length (super easy once settings page is done)
- [ ] Set keyboard shortcut for quitting (super easy once settings page is done)
- [ ] Show keyboard shortcut for quitting (easy)
- [x] Check Windows compatability
- [ ] Make AppImages and/or .deb files (no idea)
- [x] Make Windows binaries

## Contributing/ Support

If you face any issue/ have a feature request. Please make a GitLab issue for it. If you are able to code, and want to finish something on the todo, or a feature you want to be added, please feel free to make a merge request!

## Acknowledgements

Made using PyQt5, [keyboard module](https://github.com/boppreh/keyboard), [PyAutoGui](pyautogui.readthedocs.io/) and Tkinter.

## License

Autoquicker
Copyright (C) 2022  eyekay

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
