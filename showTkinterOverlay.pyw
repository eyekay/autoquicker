# Autoquicker
# Copyright © 2022 eyekay.

import tkinter as tk

top = tk.Tk()
def main():
    top.configure(bg='black')
    top.wm_overrideredirect(True)
    top.wait_visibility(top)
    top.geometry("{0}x{1}+0+0".format(top.winfo_screenwidth(), top.winfo_screenheight()))
    top.attributes('-alpha',0.4)
    top.bind('<Button-1>',exiter)
    l = tk.Label(text='Please select a position', font=("Sans", 60))
    l.pack(expand=True)
    top.mainloop()

def exiter(event):
    print(event)
    top.destroy()

if __name__ == "__main__":
    main()
