'''
Autoquicker
Copyright © 2023 eyekay.
'''

import tkinter.messagebox as messagebox
import os
import sys

# CHECKS

if os.geteuid() == 0:
    pass
else:
    messagebox.showerror('Error', 'Please run this program as root.')
    sys.exit(0)

# Create a lock file to prevent multiple instances of the program from running
if os.path.exists('/tmp/libclicker-running.lock'):
    messagebox.showerror('Error', 'Autoquicker is already running.')
    sys.exit(0)
else:
    with open('/tmp/autoquicker-running.lock', 'w') as f:
        f.write('')

from tkinter import *
from tkinter.ttk import *
import tkinter.ttk as ttk
import libclicker
import subprocess
import string
import time
import threading
import libevdev

#====================================================================================================
# MAIN WINDOW
#====================================================================================================

def change_hotkey():
    global hotkey
    top.unbind('<' + hotkey + '>')
    hotkey = 'F4'
    hotkey_start.set('Start (' + hotkey +')')
    hotkey_stop.set('Stop (' + hotkey +')')
    top.bind('<' + hotkey + '>', start_stop)

def start_stop():
    if autoclicker_running:
        stop()
    else:
        start()

def about():
    about_window = Toplevel()
    about_window.title("About Autoquicker")
    content_frame = Frame(about_window)
    content_frame.pack(fill=BOTH, expand=True, padx=10, pady=10)

    logo = PhotoImage(file="icon64.png")
    logo_label = Label(content_frame, image=logo)
    logo_label.pack()

    text = Text(content_frame, wrap=WORD, height=8)
    text.insert(INSERT, '''Autoquicker\nVersion 2.0\n\n(c) 2023 Satvik Patwardhan (eyekay)\n\nThis program is free software; \
you can redistribute it and/or modify it under the terms of the GNU General Public License as published \
by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied \
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free \
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA''')
    text.config(state=DISABLED)
    text.pack(pady=10)

    close_button = Button(content_frame, text="Close", command=about_window.destroy)
    close_button.pack(pady=10)

    about_window.transient(top)
    about_window.grab_set()
    top.wait_window(about_window)

def exit():
    if os.path.exists('/tmp/autoquicker-running.lock'):
        os.remove('/tmp/autoquicker-running.lock')
    sys.exit(0)

top = Tk()
top.title('Autoquicker')
top.resizable(False, False)
style = ttk.Style()
style.theme_use('clam')
photo = PhotoImage(file = 'icon64.png')
top.iconphoto(True, photo)

hotkey = 'F6'
hotkey_start = StringVar()
hotkey_start.set('Start (F6)')
hotkey_stop = StringVar()
hotkey_stop.set('Stop (F6)')

#====================================================================================================
# TABS
#====================================================================================================

menubar = Menu(top)
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label = 'Change Hotkey', command = lambda: messagebox.showinfo('Info', 'This feature is not yet implemented.'))
filemenu.add_command(label = 'About', command = about)
filemenu.add_separator()
filemenu.add_command(label = 'Exit', command = exit)
menubar.add_cascade(label = 'Autoquicker', menu = filemenu)
top.config(menu = menubar)

tabs = Notebook(top)
click_tab = Frame(tabs)
key_tab = Frame(tabs)
text_tab = Frame(tabs)
scroll_tab = Frame(tabs)
tabs.add(click_tab, text = 'Click')
tabs.add(key_tab, text = 'Key')
tabs.add(text_tab, text = 'Text')
tabs.add(scroll_tab, text = 'Scroll')
tabs.grid(row = 0, sticky = 'nsew', padx=5, pady=5)

# START CLICK TAB
xcoord = IntVar()
ycoord = IntVar()

def getcoords():
    event = subprocess.check_output(['python3', os.path.dirname(os.path.realpath(__file__)) + '/showTkinterOverlay.py']).decode()

    x = int(str(event)[str(event).find('x=') + 2: str(event).find('y=') -1])
    y = int(str(event)[str(event).find('y=') + 2: str(event).find('>')])

    xcoord.set(x)
    ycoord.set(y)
    

click_frame = Frame(click_tab)
click_frame.pack(side = TOP, fill = X, padx=5, pady=5)

coordslabel = Label(click_frame, text = 'Coordinates:')
coordslabel.grid(row=0, column=0, sticky='w', padx=5, pady=5)

Label(click_frame, text = '  X').grid(row=0, column=1, sticky='w')
xcoord_spinbox = Spinbox(click_frame, from_ = 0, to = 10000, textvariable = xcoord)
xcoord_spinbox.grid(row=0, column=2, sticky='w', padx=5, pady=5)

Label(click_frame, text = '  Y').grid(row=0, column=3, sticky='w')
ycoord_spinbox = Spinbox(click_frame, from_ = 0, to = 10000, textvariable = ycoord)
ycoord_spinbox.grid(row=0, column=4, sticky='w', padx=5, pady=5)

Label(click_frame, text = '  ').grid(row=0, column=5, sticky='w')
Button(click_frame, text = 'Get mouse coordinates', command = getcoords).grid(row=0, column=6, sticky='w', padx=5, pady=5)

Label(click_frame, text = 'Select Button:').grid(row=1, column=0, sticky='w', padx=5, pady=5)
button = StringVar()
button_combo = Combobox(click_frame, textvariable = button, values = ['Left', 'Middle', 'Right'], state='readonly')
button_combo.current(0)
button_combo.grid(row=1, column=2, sticky='w', padx=5, pady=5)

Label(click_frame, text = 'Type:').grid(row=2, column=0, sticky='w')
click_type = StringVar()
click_type_combo = Combobox(click_frame, textvariable = click_type, values = ['Single', 'Double', 'Triple'], state='readonly')
click_type_combo.current(0)
click_type_combo.grid(row=2, column=2, sticky='w', padx=5, pady=5)

# END CLICK TAB

# START KEY TAB

key_frame = Frame(key_tab)
key_frame.pack(side = TOP, fill = X, padx=5, pady=5)

Label(key_frame, text = 'Select Key:').grid(row=0, column=0, sticky='w', padx=5, pady=5)

keys = ['LEFTSHIFT',
    'SPACE',
    'TAB',
    'ENTER']

for i in string.ascii_letters:
    keys.append(i)
for i in string.digits:
    keys.append(i)

keys.extend(['AMPERSAND (&)', "APOSTROPHE (')", 'ASTERISK (*)',
             'AT (@)', 'BACKQUOTE (`)', 'BACKSLASH (\\)',
             'BAR (|)', 'CARET (^)', 'COLON (:)', 'COMMA (,)',
             'DOLLAR ($)', 'EQUAL (=)', 'EXCLAMATION (!)',
             'GREATER (>)', 'LEFTBRACE ({)', 'LEFTBRACKET ([)',
             'LEFTPARENTHESIS (()', 'LESS (<)', 'MINUS (-)',
             'NUMBER (#)', 'PERCENT (%)', 'PERIOD (.)', 'PLUS (+)',
             'QUESTIONMARK (?)', 'QUOTE (")', 'RIGHTBRACE (})',
             'RIGHTBRACKET (])', 'RIGHTPARENTHESIS ())', 'SEMICOLON (;)',
             'SLASH (/)', 'TILDE (~)', 'UNDERSCORE (_)'])

key = StringVar()
key_combo = Combobox(key_frame, textvariable = key, values = keys, state='readonly')
key_combo.current(0)
key_combo.grid(row=0, column=2, sticky='w', padx=5, pady=5)

# END KEY TAB

# START TEXT TAB

Label(text_tab, text = 'Enter text to type: ').pack(side = TOP, fill = X, padx=5, pady=5)
text = StringVar()
text_entry = Entry(text_tab, textvariable = text)
text_entry.pack(side = TOP, fill = X, padx=5, pady=5)
labelText = '''You can only use Latin characters found on QWERTY keyboards.
If you want to type a character that is not present on the keyboard,
you can switch to a different keyboard layout.'''
Label(text_tab, text = labelText).pack(side = TOP, fill = X, padx=5, pady=5)

# END TEXT TAB

# START SCROLL TAB

def getcoords_scroll():
    event = subprocess.check_output(['python3', os.path.dirname(os.path.realpath(__file__)) + '/showTkinterOverlay.py']).decode()

    x = int(str(event)[str(event).find('x=') + 2: str(event).find('y=') -1])
    y = int(str(event)[str(event).find('y=') + 2: str(event).find('>')])

    scroll_xcoord.set(x)
    scroll_ycoord.set(y)

coordslabel = Label(scroll_tab, text = 'Coordinates:')
coordslabel.grid(row=0, column=0, sticky='w', padx=5, pady=5)

scroll_xcoord = IntVar()
scroll_ycoord = IntVar()

Label(scroll_tab, text = '  X').grid(row=0, column=1, sticky='w')
scroll_xcoord_spinbox = Spinbox(scroll_tab, from_ = 0, to = 10000, textvariable = scroll_xcoord)
scroll_xcoord_spinbox.grid(row=0, column=2, sticky='w', padx=5, pady=5)

Label(scroll_tab, text = '  Y').grid(row=0, column=3, sticky='w')
scroll_ycoord_spinbox = Spinbox(scroll_tab, from_ = 0, to = 10000, textvariable = scroll_ycoord)
scroll_ycoord_spinbox.grid(row=0, column=4, sticky='w', padx=5, pady=5)

Label(scroll_tab, text = '  ').grid(row=0, column=5, sticky='w')
Button(scroll_tab, text = 'Get mouse coordinates', command = getcoords_scroll).grid(row=0, column=6, sticky='w', padx=5, pady=5)

Label(scroll_tab, text = 'Select Direction: ').grid(row=1, column=0, sticky='w', padx=5, pady=5)
direction = StringVar()
direction_combo = Combobox(scroll_tab, textvariable = direction, values = ['Up', 'Down', 'Left', 'Right'], state='readonly')
direction_combo.current(0)
direction_combo.grid(row=1, column=2, sticky='w', padx=5, pady=5)

Label(scroll_tab, text = 'Select Distance: ').grid(row=2, column=0, sticky='w', padx=5, pady=5)
distance = StringVar()
distance_spinbox = Spinbox(scroll_tab, from_ = 2, to = 120, textvariable = distance)
distance_spinbox.grid(row=2, column=2, sticky='w', padx=5, pady=5)
distance.set(2)

# END SCROLL TAB

# END TABS
#====================================================================================================


#====================================================================================================
# OPTIONS including duration, interval and control buttons
#====================================================================================================

autoclicker_running = False

starting_time = 0
ending_time = 0

def click_thread_():
    while autoclicker_running:
        if fix_duration.get() and time.time() > ending_time:
            stop()
        libclicker.click(int(xcoord_spinbox.get()), int(ycoord_spinbox.get()), button_combo.current() , click_type_combo.current() + 1)
        time.sleep(interval)

def key_thread_():
    while autoclicker_running:
        if fix_duration.get() and time.time() > ending_time:
            stop()
        libclicker.press_key(key_combo.get())
        time.sleep(interval)

def text_thread_():
    while autoclicker_running:
        if fix_duration.get() and time.time() > ending_time:
            stop()
        libclicker.type_text(text.get())
        time.sleep(interval)

def scroll_thread_():
    while autoclicker_running:
        if fix_duration.get() and time.time() > ending_time:
            stop()
        libclicker.scroll(int(scroll_xcoord.get()), int(scroll_ycoord.get()), int(distance.get()), direction_combo.get())
        time.sleep(interval)

def start():
    global autoclicker_running
    global interval
    global starting_time
    global ending_time

    subprocess.run(['python3', os.path.dirname(os.path.realpath(__file__)) + '/showCountOverlay.py'])

    if fix_duration.get():
        starting_time = time.time()
        ending_time = starting_time + (duration_minutes.get() * 60) + duration_seconds.get() + (duration_milliseconds.get() / 1000)

    interval = interval_minutes.get() * 60 + interval_seconds.get() + interval_milliseconds.get() / 1000
    if interval < 0.001:
        messagebox.showerror('Error', 'Your interval is too short to be accurate. Please increase it.')
        return
    if tabs.index(tabs.select()) == 0:
        autoclicker_running = True
        click_thread = threading.Thread(target = click_thread_)
        click_thread.start()
    elif tabs.index(tabs.select()) == 1:
        autoclicker_running = True
        key_thread = threading.Thread(target = key_thread_)
        key_thread.start()
    elif tabs.index(tabs.select()) == 2:
        autoclicker_running = True
        text_thread = threading.Thread(target = text_thread_)
        text_thread.start()
    elif tabs.index(tabs.select()) == 3:
        autoclicker_running = True
        scroll_thread = threading.Thread(target = scroll_thread_)
        scroll_thread.start()

def stop():
    global autoclicker_running
    autoclicker_running = False
    if tabs.index(tabs.select()) == 0:
        messagebox.showinfo('Info', 'Clicking has finished.')
    elif tabs.index(tabs.select()) == 1:
        messagebox.showinfo('Info', 'Key press automation has finished.')
    elif tabs.index(tabs.select()) == 2:
        messagebox.showinfo('Info', 'Text typing has finished.')
    elif tabs.index(tabs.select()) == 3:
        messagebox.showinfo('Info', 'Scrolling has finished.')

footer_frame = Frame(top)
footer_frame.grid(row = 1, sticky = 'nsew', padx=5, pady=5)

# START COMMON OPTIONS FRAME

common_options_frame = Frame(footer_frame)
common_options_frame.pack(side = TOP, fill = X, padx=5, pady=5)

# START DURATION FRAME

duration_frame = Frame(common_options_frame, borderwidth = 2, relief = 'groove')
duration_frame.grid(row = 0, column = 0, sticky='ew', padx=5, pady=5)

fix_duration = BooleanVar()
fix_duration_checkbox = Checkbutton(duration_frame, text = 'Fix duration?', variable = fix_duration)
fix_duration_checkbox.pack(side = LEFT, padx=5, pady=5)
def fix_duration_checkbox_callback():
    if fix_duration.get() == 1:
        duration_hours_spinbox.config(state = 'normal')
        duration_minutes_spinbox.config(state = 'normal')
        duration_seconds_spinbox.config(state = 'normal')
        duration_milliseconds_spinbox.config(state = 'normal')
    else:
        duration_hours_spinbox.config(state = 'disabled')
        duration_minutes_spinbox.config(state = 'disabled')
        duration_seconds_spinbox.config(state = 'disabled')
        duration_milliseconds_spinbox.config(state = 'disabled')
fix_duration_checkbox.config(command = fix_duration_checkbox_callback)

duration_hours = IntVar()
duration_hours_spinbox = Spinbox(duration_frame, from_ = 0, to = 23, textvariable = duration_hours, width = 5)
duration_hours_spinbox.pack(side = LEFT, padx=5, pady=5)
duration_hours_spinbox.config(state = 'disabled')
Label(duration_frame, text = 'h').pack(side = LEFT, padx=5, pady=5)
duration_minutes = IntVar()
duration_minutes_spinbox = Spinbox(duration_frame, from_ = 0, to = 59, textvariable = duration_minutes, width = 5)
duration_minutes_spinbox.pack(side = LEFT, padx=5, pady=5)
duration_minutes_spinbox.config(state = 'disabled')
Label(duration_frame, text = 'm').pack(side = LEFT, padx=5, pady=5)
duration_seconds = IntVar()
duration_seconds_spinbox = Spinbox(duration_frame, from_ = 0, to = 59, textvariable = duration_seconds, width = 5)
duration_seconds_spinbox.pack(side = LEFT, padx=5, pady=5)
duration_seconds_spinbox.config(state = 'disabled')
Label(duration_frame, text = 's').pack(side = LEFT, padx=5, pady=5)
duration_milliseconds = IntVar()
duration_milliseconds_spinbox = Spinbox(duration_frame, from_ = 0, to = 999, textvariable = duration_milliseconds, width = 5)
duration_milliseconds_spinbox.pack(side = LEFT, padx=5, pady=5)
duration_milliseconds_spinbox.config(state = 'disabled')
Label(duration_frame, text = 'ms').pack(side = LEFT, padx=5, pady=5)

# END DURATION FRAME

# START INTERVAL FRAME

interval_frame = Frame(common_options_frame, borderwidth = 2, relief = 'groove')
interval_frame.grid(row = 1, column= 0, sticky='ew', padx=5, pady=5)

Label(interval_frame, text = 'Interval:').pack(side = LEFT, padx=5, pady=5)
Label(interval_frame, text = ' ').pack(side = LEFT, fill=X, expand=1, padx=5, pady=5)
interval_minutes = IntVar()
interval_minutes_spinbox = Spinbox(interval_frame, from_ = 0, to = 9, textvariable = interval_minutes, width = 5)
interval_minutes_spinbox.pack(side = LEFT, padx=5, pady=5)
Label(interval_frame, text = 'm').pack(side = LEFT, padx=5, pady=5)
interval_seconds = IntVar()
interval_seconds.set(1)
interval_seconds_spinbox = Spinbox(interval_frame, from_ = 0, to = 59, textvariable = interval_seconds, width = 5)
interval_seconds_spinbox.pack(side = LEFT, padx=5, pady=5)
Label(interval_frame, text = 's').pack(side = LEFT, padx=5, pady=5)
interval_milliseconds = IntVar()
interval_milliseconds_spinbox = Spinbox(interval_frame, from_ = 0, to = 999, textvariable = interval_milliseconds, width = 5)
interval_milliseconds_spinbox.pack(side = LEFT, padx=5, pady=5)
Label(interval_frame, text = 'ms').pack(side = LEFT, padx=5, pady=5)

# END INTERVAL FRAME

# END COMMON OPTIONS FRAME

# START CONTROL FRAME

control_frame = Frame(common_options_frame)
control_frame.grid(row = 0, column = 1, rowspan = 2, sticky = 'nsew', padx=5, pady=5)

start_button = Button(control_frame, textvariable = hotkey_start, command = start)
start_button.pack(side = TOP, fill = BOTH, padx=5, pady=5)
stop_button = Button(control_frame, textvariable = hotkey_stop, command = stop)
stop_button.pack(side = TOP, fill = BOTH, padx=5, pady=5)

# END CONTROL FRAME

# END OPTIONS
#====================================================================================================

# STARTUP

# Start monitoring function for hotkey
def hotkey_monitor():
    for filename in os.listdir('/dev/input'):
        if filename.startswith('event'):
            path = os.path.join('/dev/input', filename)
            fd = open(path, 'rb')
            try:
                device = libevdev.Device(fd)
            except:
                continue
            if device.name.lower().find('keyboard') > -1:
                fd.close()
                break
    fd = open(path, 'rb')
    d = libevdev.Device(fd)

    while True:
        for e in d.events():
            if not e.matches(libevdev.EV_KEY):
                continue
            if e.matches(libevdev.EV_KEY.KEY_F6, 1):
                start_stop()

hotkey_monitor_thread = threading.Thread(target = hotkey_monitor)
hotkey_monitor_thread.daemon = True
hotkey_monitor_thread.start()

top.mainloop()