# Autoquicker
# Copyright © 2022 eyekay.

import sys
import os
import sys
import subprocess

# keyboard module doesn't work without being run as superuser.

if sys.platform in ('linux','linux2'):
    if os.geteuid() != 0:
        subprocess.call(['xmessage','Please run Autoquicker as superuser. This program requires superuser permissions as a safety measure.'])
        exit(1)

try: #Thanks to https://www.pythonguis.com/tutorials/packaging-pyqt5-pyside2-applications-windows-pyinstaller/
    from ctypes import windll  # Only exists on Windows.
    myappid = 'io.gitlab.eyekay.autoquicker.1.0'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
except ImportError:
    pass


from PyQt5 import QtWidgets, QtCore
import mainwindow
import keyboard
import pyautogui
import time

def exiter():
    my_mainWindow.close()
keyboard.add_hotkey('shift+f8', lambda: exiter())

# TODO
# Rewrite screen overlays in Qt to remove Tk dependency

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = mainwindow.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.startbutton.clicked.connect(self.startAutomation)
        self.ui.keyboardcheckBox.clicked.connect(self.enableKeyboards)
        self.ui.mousecheckBox.clicked.connect(self.enableMice)
        self.ui.fixdurationcheckBox.clicked.connect(self.enableDuration)
        self.ui.selectposbutton.clicked.connect(self.selectClickPosition)
        self.ui.stopbutton.clicked.connect(self.close)
        self.ui.keypressradioButton.clicked.connect(self.enableKeys)
        self.ui.typetextradiobutton.clicked.connect(self.enableType)
        self.ui.actionAbout_Qt.triggered.connect(lambda: self.showAboutQt())
        self.ui.actionAbout.triggered.connect(lambda: self.showAbout())

        #enable stuff by default
        self.ui.mousecheckBox.setChecked(True)
        self.ui.buttoncomboBox.setEnabled(True)
        self.ui.buttontypecombobox.setEnabled(True)
        self.ui.xspinbox.setEnabled(True)
        self.ui.yspinbox.setEnabled(True)
        self.ui.selectposbutton.setEnabled(True)

    def enableKeyboards(self):
        self.ui.mousecheckBox.setChecked(False)
        self.ui.buttoncomboBox.setEnabled(False)
        self.ui.buttontypecombobox.setEnabled(False)
        self.ui.xspinbox.setEnabled(False)
        self.ui.yspinbox.setEnabled(False)
        self.ui.selectposbutton.setEnabled(False)

        if self.ui.keyboardcheckBox.isChecked() == True:
            self.ui.keypressradioButton.setEnabled(True)
            self.ui.keyboardkeycombobox.setEnabled(True)
            self.ui.typetextradiobutton.setEnabled(True)

        if self.ui.keyboardcheckBox.isChecked() == False:
            self.ui.keypressradioButton.setEnabled(False)
            self.ui.keyboardkeycombobox.setEnabled(False)
            self.ui.typetextradiobutton.setEnabled(False)

    def enableKeys(self):
        if self.ui.keypressradioButton.isChecked() == True:
            self.ui.keyboardkeycombobox.setEnabled(True)
            self.ui.lineEdit.setEnabled(False)
        if self.ui.keypressradioButton.isChecked() == False:
            self.ui.keyboardkeycombobox.setEnabled(False)

    def enableType(self):
        if self.ui.typetextradiobutton.isChecked() == True:
            self.ui.lineEdit.setEnabled(True)
            self.ui.keyboardkeycombobox.setEnabled(False)
        if self.ui.typetextradiobutton.isChecked() == False:
            self.ui.lineEdit.setEnabled(False)

    def enableMice(self):
        self.ui.keyboardcheckBox.setChecked(False)
        self.ui.keypressradioButton.setEnabled(False)
        self.ui.keyboardkeycombobox.setEnabled(False)
        self.ui.typetextradiobutton.setEnabled(False)

        if self.ui.mousecheckBox.isChecked() == True:
            self.ui.buttoncomboBox.setEnabled(True)
            self.ui.buttontypecombobox.setEnabled(True)
            self.ui.xspinbox.setEnabled(True)
            self.ui.yspinbox.setEnabled(True)
            self.ui.selectposbutton.setEnabled(True)

        if self.ui.mousecheckBox.isChecked() == False:
            self.ui.buttoncomboBox.setEnabled(False)
            self.ui.buttontypecombobox.setEnabled(False)
            self.ui.xspinbox.setEnabled(False)
            self.ui.yspinbox.setEnabled(False)
            self.ui.selectposbutton.setEnabled(False)

    def enableDuration(self):
        if self.ui.fixdurationcheckBox.isChecked() == True:
            self.ui.hoursspinBox.setEnabled(True)
            self.ui.minutesspinBox.setEnabled(True)
            self.ui.secondsspinBox.setEnabled(True)
            self.ui.millisecondsspinBox.setEnabled(True)

        if self.ui.fixdurationcheckBox.isChecked() == False:
            self.ui.hoursspinBox.setEnabled(False)
            self.ui.minutesspinBox.setEnabled(False)
            self.ui.secondsspinBox.setEnabled(False)
            self.ui.millisecondsspinBox.setEnabled(False)

    def selectClickPosition(self):
        if sys.platform in ('linux','linux2'):
            pwd=subprocess.check_output('pwd')
            pwd = pwd.decode('UTF-8')[:-1]
            cmd = 'python3'
        elif sys.platform == 'win32':
            pwd=os.getcwd()
            cmd = 'pythonw'
        output = subprocess.check_output([cmd, pwd + '/showTkinterOverlay.pyw']) # This gives different outputs on Windows and Linux. On Windows, it adds an extra carriage return at the end.
        output = output.decode('UTF-8')[:-1] 
        x = output[27:]
        tmpstr=''
        for i in range(len(x)):
            if x[i].isdigit() == True:
                tmpstr = tmpstr +x[i]
            else:
                break
        x = tmpstr
        y = output[27:]
        y = y.lstrip(x)
        if sys.platform in ('linux','linux2'):
            y = y[3:-1]
        elif sys.platform == 'win32':
            y = y[3:-2] # Windows gives an extra carriage return.
        self.ui.xspinbox.setValue(int(x))
        self.ui.yspinbox.setValue(int(y))

    def startAutomation(self):
        if sys.platform in ('linux','linux2'):
            pwd=subprocess.check_output('pwd')
            pwd = pwd.decode('UTF-8')[:-1]
            cmd = 'python3'
        elif sys.platform == 'win32':
            pwd=os.getcwd()
            cmd = 'pythonw'
        subprocess.call([cmd, pwd + '/showCountOverlay.pyw'])

        if self.ui.mousecheckBox.isChecked() == True:
            if self.ui.buttoncomboBox.currentText() == 'Left':
                if self.ui.buttontypecombobox.currentText() == 'Single':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('LEFT',1, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('LEFT',1, True)
                elif self.ui.buttontypecombobox.currentText() == 'Double':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('LEFT',2, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('LEFT',2, True)
            elif self.ui.buttoncomboBox.currentText() == 'Middle (Scroll Wheel)':
                if self.ui.buttontypecombobox.currentText() == 'Single':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('MIDDLE',1, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('MIDDLE',1, True)
                elif self.ui.buttontypecombobox.currentText() == 'Double':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('MIDDLE',2, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('MIDDLE',2, True)
            elif self.ui.buttoncomboBox.currentText() == 'Right':
                if self.ui.buttontypecombobox.currentText() == 'Single':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('RIGHT',1, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('RIGHT',1, True)
                elif self.ui.buttontypecombobox.currentText() == 'Double':
                    if self.ui.fixdurationcheckBox.isChecked() == False:
                        self.click('RIGHT',2, False)
                    elif self.ui.fixdurationcheckBox.isChecked() == True:
                        self.click('RIGHT',2, True)
        elif self.ui.keyboardcheckBox.isChecked() == True:
            if self.ui.keypressradioButton.isChecked() == True:
                self.pressthekey(self.ui.keyboardkeycombobox.currentText())
            elif self.ui.typetextradiobutton.isChecked() == True:
                self.typetext(self.ui.lineEdit.text())

    def click(self,button,type,duration):
        # interval is in seconds
        interval = self.ui.inthoursspinBox.value()*60*60+self.ui.intminutesspinBox.value()*60+self.ui.intsecondsspinBox.value()+self.ui.intmillisecondsspinBox.value()*0.001
        print('button',button,'type',type,'interval',interval)
        if duration == False:
            self.actuallyClickNow(button,type)
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: self.actuallyClickNow(button,type))
        elif duration == True:
            self.actuallyClickNow(button,type)
            durint = self.ui.hoursspinBox.value()*60*60+self.ui.minutesspinBox.value()*60+self.ui.secondsspinBox.value()+self.ui.millisecondsspinBox.value()*0.001
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: self.actuallyClickNow(button,type))
            self.duratimer = QtCore.QTimer()
            self.duratimer.setSingleShot(True)
            self.duratimer.start(int(durint*1000))
            self.duratimer.timeout.connect(lambda: exiter())

    def actuallyClickNow(self,button,type):
        posx = self.ui.xspinbox.value()
        posy = self.ui.yspinbox.value()
        pyautogui.click(x=(posx,posy),clicks=type,button=button)

    def pressthekey(self,key):
        topress=''
        combos={'Tab':'\t','Space':' ','- (Hyphen/ Minus)':'-','_ (Underscore)':'_','Alt (Generic)':'alt','Alt (Left)':'altleft','Alt (Right)':'altright','Backspace':'backspace','Caps Lock':'capslock','Ctrl (Generic)':'ctrl','Ctrl (Left)':'ctrlleft','Ctrl (Right)':'ctrlright','Delete':'delete','Down':'down','End':'end','Enter':'enter','Escape':'esc','F1':'f1','F2':'f2','F3':'f3','F4':'f4','F5':'f5','F6':'f6','F7':'f7','F8':'f8','F9':'f9','F10':'f10','F11':'f11','F12':'f12','Home':'home','Insert':'insert','Left':'left','Meta (Windows) Key':'win','Num Lock':'numlock','Page Down':'pagedown','Page Up':'pageup','Print Screen':'printscreen','Return':'return','Right':'right','Scroll Lock':'scrolllock','Shift (Generic)':'shift','Shift (Left)':'shiftleft','Shift (Right)':'shiftright','Sleep':'sleep','Super (Windows) Key':'win','Up':'up','Windows (Super/ Meta) Key':'win','[Newline]':'\n','[Carriage Return]':'\r'}
        if key in combos:
            topress=combos[key]
        else:
            topress=key
        interval = self.ui.inthoursspinBox.value()*60*60+self.ui.intminutesspinBox.value()*60+self.ui.intsecondsspinBox.value()+self.ui.intmillisecondsspinBox.value()*0.001
        if self.ui.fixdurationcheckBox.isChecked() == True:
            pyautogui.press(topress)
            durint = self.ui.hoursspinBox.value()*60*60+self.ui.minutesspinBox.value()*60+self.ui.secondsspinBox.value()+self.ui.millisecondsspinBox.value()*0.001
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: pyautogui.press(topress))
            self.duratimer = QtCore.QTimer()
            self.duratimer.setSingleShot(True)
            self.duratimer.start(int(durint*1000))
            self.duratimer.timeout.connect(lambda: exiter())
        elif self.ui.fixdurationcheckBox.isChecked()== False:
            pyautogui.press(topress)
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: pyautogui.press(topress))

    def typetext(self,text):
        interval = self.ui.inthoursspinBox.value()*60*60+self.ui.intminutesspinBox.value()*60+self.ui.intsecondsspinBox.value()+self.ui.intmillisecondsspinBox.value()*0.001
        if self.ui.fixdurationcheckBox.isChecked() == True:
            pyautogui.write(text)
            durint = self.ui.hoursspinBox.value()*60*60+self.ui.minutesspinBox.value()*60+self.ui.secondsspinBox.value()+self.ui.millisecondsspinBox.value()*0.001
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: pyautogui.write(text))
            self.duratimer = QtCore.QTimer()
            self.duratimer.setSingleShot(True)
            self.duratimer.start(int(durint*1000))
            self.duratimer.timeout.connect(lambda: exiter())
        elif self.ui.fixdurationcheckBox.isChecked()== False:
            pyautogui.write(text)
            self.timer = QtCore.QTimer()
            self.timer.start(int(interval*1000))
            self.timer.timeout.connect(lambda: pyautogui.write(text))

    def showAboutQt(self):
        aboutqtscrn=QtWidgets.QMessageBox.aboutQt(self,'Autoquicker')

    def showAbout(self):
        aboutscrn=QtWidgets.QMessageBox.about(self,'About Autoquicker','<b>Autoquicker</b><br>version 1.0<br>Copyright © 2022 eyekay<br>This program uses the Python libraries PyQt5, Tkinter, PyAutoGui and keyboard.')

app = QtWidgets.QApplication(sys.argv)
my_mainWindow = MainWindow()
my_mainWindow.show()
sys.exit(app.exec_())
