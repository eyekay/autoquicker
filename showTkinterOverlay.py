# Autoquicker
# Copyright © 2022 eyekay.

import tkinter as tk

overlay = tk.Tk()
def main():
    overlay.configure(bg='black')
    overlay.wm_overrideredirect(True)
    overlay.wait_visibility(overlay)
    overlay.geometry("{0}x{1}+0+0".format(overlay.winfo_screenwidth(), overlay.winfo_screenheight()))
    overlay.attributes('-alpha',0.4)
    overlay.bind('<Button-1>',exiter)
    overlay.mainloop()

def exiter(event):
    print(event)
    overlay.destroy()

if __name__ == "__main__":
    main()
