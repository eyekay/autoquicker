# Autoquicker
# Copyright © 2022 eyekay.

from tkinter import *

def main():
	top = Tk()
	top.configure(bg='black')
	top.wm_overrideredirect(True)
	top.wait_visibility(top)
	top.geometry("{0}x{1}+0+0".format(top.winfo_screenwidth(), top.winfo_screenheight()))
	top.attributes('-alpha',0.4)
	prefix = 'Starting in '
	l = Label(text=prefix+'5...', font=("Sans", 60))
	l.pack(expand=True)
	l.after(1000, lambda: l.configure(text=prefix+'4...'))
	l.after(2000, lambda: l.configure(text=prefix+'3...'))
	l.after(3000, lambda: l.configure(text=prefix+'2...'))
	l.after(4000, lambda: l.configure(text=prefix+'1...'))
	l.after(5000, lambda: top.destroy())
	top.mainloop()

main()